// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;          // variable to store the servo position 
int resolution = 30;  // angular resolution
int led = 9;          // Pin 9 has an LED connected on Arduino Ethernet board
int long_delay_ms = 1000;
int short_delay_ms = 100;

void setup() 
{ 
  pinMode(led, OUTPUT);  // initialize the digital pin as an output
  myservo.attach(6);     // attaches the servo on pin 9 to the servo object 
} 
 
void flikker(int blinks, int delay_ms)
{
  for (int i = 0; i < blinks; i += 1)
  {
    digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(delay_ms);                       // waits for the servo to reach the position 
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    delay(delay_ms);                       // waits for the servo to reach the position 
  }
}

void loop() 
{ 
  for(pos = 0; pos < 2; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos*resolution);   // tell servo to go to position in variable 'pos' 
    delay(short_delay_ms);                       // waits for the servo to reach the position 
    flikker(10, 60);
  } 
/*
  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
*/ 
} 
