#include <SPI.h>
#include <Ethernet.h>

/* =================================================================== */
// ------------------------- TEMPERATUUR ----------------------------- */
/* =================================================================== */
#define ADO_NOMMER_BY_5V 204.8
#define FOUT             -1.0
#define R1_OHM           10000.0
#define TABELGROOTTE     101
#define TERMISTOR_KANAAL 0
#define VCC_VOLT         5.0
#define TEMP_HOOG        22.0

/* Weerstand van DO-35 Series termistor teenoor temperatuur. Begin by 0,
   en eindig by 100 grade Celsius. Posisie in die skikking = temperatuur.
   Bron: DO-35 Series Termistor in datavel "DO-35 Series Thermistor –
   50KF3950DPGN" van Measurement Specialties.                          */
float weerstand_tabel[TABELGROOTTE] = {160.8156, 152.9365, 145.4894,
  138.4480, 131.7882, 125.4871, 119.5233, 113.8772, 108.5298, 103.4640,
  98.6632, 94.1123, 89.7968, 85.7034, 81.8194, 78.1330, 74.6332, 71.3094,
  68.1519, 65.1516, 62.2997, 59.5883,57.0095, 54.5563, 52.2220, 50.0000,
  47.8845, 45.8698, 43.9506, 42.1219, 40.3790, 38.7173, 37.1328, 35.6215,
  34.1795,32.8035, 31.4900, 30.2359, 29.0382, 27.8941, 26.8009, 25.7562,
  24.7576, 23.8027, 22.8896, 22.0160, 21.1802, 20.3804, 19.6147, 18.8817,
  18.1797, 17.5094,16.8671, 16.2514, 15.6613, 15.0955, 14.5529, 14.0324,
  13.5331, 13.0539, 12.5940,12.1526, 11.7287, 11.3217, 10.9307, 10.5551,
  10.1942, 9.8474, 9.5140, 9.1935, 8.8853, 8.5888, 8.3037, 8.0294, 7.7654,
  7.5113, 7.2668, 7.0313, 6.8046, 6.5862, 6.3758, 6.1732, 5.9779, 5.7897,
  5.6083, 5.4334, 5.2647, 5.1021, 4.9452, 4.7938, 4.6478, 4.5069, 4.3709,
  4.2396, 4.1129,3.9905, 3.8723, 3.7581, 3.6478, 3.5413, 3.4383};

// Skakel die termistor weerstand om na temperatuur.
float r_na_t(float weerstand_kohm) {
  float wsi, wsi1;

  for (int i = 0; i < TABELGROOTTE - 1; i++) {
    wsi  = (float)weerstand_tabel[i];
    wsi1 = (float)weerstand_tabel[i+1];
    if ((weerstand_kohm <= wsi) && (weerstand_kohm >= wsi1)) {
     return i + 1 * (wsi1 - weerstand_kohm) / (wsi - wsi1);
    } 
  }
  return FOUT;
}

// Skakel die spanning oor die termistor om na die termistor weerstand.
// Bereken die weerstand van die termistor. Die stroombaan bestaan uit
// 'n konstante weerstand, R1 in serie met die termistor, RT. Daar is 
// 'n spanningsval Vcc oor die RT en R1 seriebaan.
float v_na_r(float termistor_spanning) {
 return R1_OHM / ((VCC_VOLT / termistor_spanning) - 1);
}

// Skakel die ADO getal om na 'n spanning oor die termistor.
float ado_na_v(int ado_getal) {
 return (float)ado_getal / ADO_NOMMER_BY_5V; 
}

// Lees die Analoog na Digitaal Omsetter (ADO) inset op 'n kanaal 0.
// Skakel die ADO getal om na 'n spanning oor die termistor.
// Skakel die spanning oor die termistor om na die termistor weerstand.
// Skakel die termistor weerstand om na temperatuur.
float meet_temperatuur(int kanaal) {
 return r_na_t(v_na_r(ado_na_v(analogRead(kanaal)))/1000.0);
}


/* =================================================================== */
// -------------------------- WEB SERVER ----------------------------- */
/* =================================================================== */
/*
 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield. 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)
 
 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 */

#define WEBSERVER_PORT 80

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,2,44);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(WEBSERVER_PORT);

void setup() {
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
	  client.println("Refresh: 1");  // refresh the page automatically every 1 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");

          // output temperature
          /* debug begin
          client.print("Spanning  = ");
          client.print(ado_na_v(analogRead(TERMISTOR_KANAAL)));
          client.println(" V<br />");
          client.print("Weerstand = ");
          client.print(v_na_r(ado_na_v(analogRead(TERMISTOR_KANAAL))));
          client.println(" ohm<br />");
          debug eindig*/
          
          client.print("Temperatuur (op analoog inset ");
          client.print(TERMISTOR_KANAAL);
          client.print(") is  <b>");
          client.print(meet_temperatuur(TERMISTOR_KANAAL));  
          client.print(" <sup>o</sup></b></font>");
          client.println("<br />");

          /*
          // output the value of each analog input pin
          for (int analogChannel = 0; analogChannel < 6; analogChannel++) {
            int sensorReading = analogRead(analogChannel);
            client.print("analog input ");
            client.print(analogChannel);
            client.print(" is ");
            client.print(sensorReading);
            client.println("<br />");       
          }
          */
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}

